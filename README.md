# keycloak
This repo for welive platform and apis.

### Requirements
* [Docker](https://www.docker.com/)
* [docker-compose](https://github.com/docker/compose/releases)


### Refs
* [Home](http://www.keycloak.org/)
* [Blog](http://blog.keycloak.org/)
* [User.Guide](https://keycloak.gitbooks.io/documentation/getting_started/index.html)
